FROM php:8.0-apache-buster

RUN apt-get -y update

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Git
RUN apt-get -y install git

# Installing network debugging tools
RUN apt-get -y install apt-utils zip telnet iputils-ping vim build-essential

# Installing php-bcmath
RUN docker-php-ext-install bcmath

# Installing php-bz2
RUN apt-get install -y libbz2-dev
RUN docker-php-ext-install bz2

# Installing php database module
RUN docker-php-ext-install pdo pdo_mysql mysqli

# Installing php exif
RUN docker-php-ext-install exif

# Installing php-gd
RUN apt-get install -y libjpeg62-turbo-dev
RUN apt-get -y install libpng-dev
RUN docker-php-ext-configure gd \
    --with-jpeg
RUN docker-php-ext-install gd

# Installing php-mbstring
RUN apt-get install -y libonig-dev
RUN docker-php-ext-install mbstring

# Installing php-opcache
RUN docker-php-ext-install opcache
# Configure php-opcache
ENV OPCACHE_ENABLE 1
ENV OPCACHE_MEMORY_COMSUMPTION 256
ENV OPCACHE_MAX_ACCELERATED_FILES 20000
ENV OPCACHE_REVALIDATE_FREQ 60
ENV OPCACHE_VALIDATE_TIMESTAMPS 0
ENV OPCACHE_INTERNED_STRINGS_BUFFER 8
ENV OPCACHE_FAST_SHUTDOWN 1

RUN echo "[opcache]" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.enable=\${OPCACHE_ENABLE}" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "# maximum memory that OPcache can use to store compiled PHP files, Symfony recommends 256" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.memory_consumption=\${OPCACHE_MEMORY_COMSUMPTION}" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "# maximum number of files that can be stored in the cache" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.max_accelerated_files=\${OPCACHE_MAX_ACCELERATED_FILES}" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "# validate on every request" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.revalidate_freq=\${OPCACHE_REVALIDATE_FREQ}" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "# re-validate of timestamps, is set to false by default, is overridden in local docker-compose" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.validate_timestamps=\${OPCACHE_VALIDATE_TIMESTAMPS}" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.interned_strings_buffer=\${OPCACHE_INTERNED_STRINGS_BUFFER}" >> $PHP_INI_DIR/conf.d/opcache.ini
RUN echo "opcache.fast_shutdown=\${OPCACHE_FAST_SHUTDOWN}" >> $PHP_INI_DIR/conf.d/opcache.ini

# Installing phpredis
RUN pecl install redis \
    && docker-php-ext-enable redis

# Installing php-xml
RUN apt-get install -y libxml2-dev
RUN docker-php-ext-install xml

# Installing php-zip
RUN apt-get -y install zlib1g-dev libzip-dev
RUN docker-php-ext-install zip

# Installing rewrite module
RUN a2enmod rewrite

#Installing Image Magick
RUN apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN printf "\n" | pecl install imagick
RUN docker-php-ext-enable imagick

COPY . /var/www/html/
