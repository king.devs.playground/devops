#! /bin/bash

apt-get update -y

# Installing docker
apt-get install docker.io -y
systemctl start docker
systemctl enable docker

# Installing kubernetes
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get install kubeadm kubelet kubectl -y
apt-mark hold kubeadm kubelet kubectl

swapoff –a

{ ##### KUBEADM JOIN COMMAND ##### }
