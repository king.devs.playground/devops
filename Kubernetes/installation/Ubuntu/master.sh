#! /bin/bash

sudo hostnamectl set-hostname master01

apt-get update -y

# Installing docker
apt-get install docker.io -y
systemctl start docker
systemctl enable docker

# Installing kubernetes
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get install kubeadm kubelet kubectl -y
apt-mark hold kubeadm kubelet kubectl

swapoff –a

systemctl start kubelet
systemctl enable kubelet

kubeadm init --pod-network-cidr=10.244.0.0/16
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

# Installing flannel
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# Installing Helm
curl https://helm.baltorepo.com/organization/signing.asc | sudo apt-key add -
apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
apt-get update
apt-get install helm

helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update

helm install my-release nginx-stable/nginx-ingress
